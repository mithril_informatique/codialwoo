# CodialWoo
Synchronisation des produits et des bons de commande entre Woocommerce et Codial

## Getting started
La synchronisation se fait via l'API de Woocommerce et des fichiers XML et CSV. Il faut renseigner les clés APIs dans `config/config.yml`.

## Installation 
Il faut installer `Ruby 2.7`.

Installation des dépendances ruby `bundle install`.

Mise à jour des dépendances  ruby `bundle update`.

## Licence
Licence AGPL v3


