require 'json'
require 'csv'
require 'yaml'

def order_data_to_csv(order, sep)
    billing = order['billing']
    order_data = [
        "[CODIALDEMO]", # Nom du site (entre crochet « [] »)
        "[ORDERS]",     # Type de ligne
        order['id'],    # Id de la commande du site
        '',             # Id du client sur le site (Laisser à vide sauf indication contraire.)
        '',             # Civilité de facturation # Absent dans order
        '',             # Nom de la société de facturation # Pas dans order
        billing['last_name'], # Nom du client de facturation
        billing['firt_name'], # Prénom du client de facturation
        billing['address_1'], # Adresse de facturation
        billing['city'],      # Ville de facturation
        billing['postcode'],  # Code postal de facturation
        billing['country'],   # Pays de facturation
        billing['email'],     # Email
        billing['phone'],     # Téléphone de facturation
        '', # Nom de la société de livraison # Pas dans order via shipping
        '', # Nom du client de livraison # Dans order mais vide via shipping
        '', # Prénom du client de livraison # Dans order mais vide via shipping
        '', # Adresse de livraison # Dans order mais vide via shipping
        '', # Code postal de livraison # Dans order mais vide via shipping
        '', # Ville de livraison # Dans order mais vide via shipping
        '', # Pays de livraison # Dans order mais vide via shipping
        order['total'],                # Total de la commande
        order['payment_method_title'], # Mode de règlement
        order['customer_note'],        # Commentaire
        '', # Type de livraison
        '', # Date de livraison prévue
        '', # Montant des frais de port
        '', # Nom du site
        billing['address_2'],          # Complément d’adresse de facturation
        '', # Complément d’adresse de livraison
        '', # Montant du chèque cadeau
        '', # Code du chèque cadeau
        '', # Code du coupon
        '', # Montant du coupon
        '', # Montant de la remise
        '', # Téléphone portable
        ''  # Code point relais
    ]
    order_csv = order_data.join(sep)
    return  order_csv
end

def product_data_to_csv(order, product, sep)
        product_data = [
            "[CODIALDEMO]", # Nom du site (entre crochet « [] »)
            "[OITEMS]",     # Type de ligne
            order['id'],    # Id de la ligne de commande du site (peut rester vide)
            '',             # Référence Codial de l’article # Comment ?
            product['product_id'], # Id de l’article sur le site (peut rester vide)
            product['name'],       # Désignation de l’article (peut rester vide)
            product['quantity'],   # Quantité commandée
            product['total'] ,     # Prix unitaire net TTC (Pris en compte en mode BtoC)
            '',                    # Id de l’option taille ou couleur (peut rester vide)
            '', # Prix unitaire net HT (Pris en compte en mode BtoB) # Pas compris c'est quel champ
        ]
        product_csv = product_data.join(sep)
        return product_csv
end

def export_orders_to_CSV(file, orders, sep=";")
    for order in orders do
        order_csv = order_data_to_csv(order, sep)
        File.write(file, order_csv, mode: "a")
        File.write(file, "\n", mode:"a")

        products = order['line_items']
        for product in products do
            product_csv = product_data_to_csv(order, product, sep)
            File.write(file, product_csv , mode: "a")
            File.write(file, "\n", mode:"a")
        end
    
        goWoocommerce("put", "orders/#{order['id']}", {status:'on-hold'})
    end
    if File.exist?(file) and !File.zero?(file) and !orders.empty?
        puts 'OrderID'
    end
end