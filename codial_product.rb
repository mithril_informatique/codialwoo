require 'net/http'
require 'uri'

class CodialProduct
  def initialize(xml_data_art)
    @name = xml_data_art['cname'].first
    @ref = xml_data_art['ccode'].first
    @sku = @ref
    @slug = xml_data_art['cname'].first
    @type = 'simple'
    @regular_price = (xml_data_art['cprice'].first.to_f * 0.7).round(2).to_s

    desc = xml_data_art['desc'].first.gsub(';">', '">').gsub('<wbr>', '')
    @description = desc

    cats = []
    xml_data_art['cat_web']&.first&.split(Regexp.new(',| '))&.each do |cat_id|
      cats << { id: cat_id }
    end
    @categories = cats

    @manage_stock = true
    qtity = xml_data_art['cstock'].first.to_i
    @stock_quantity = qtity
    @stock_status = qtity.positive? ? 'instock' : 'outofstock'

    @images = []
  end

  def image_exists?(url)
    uri = URI(url)
    res = Net::HTTP.get_response(uri)
    res.is_a?(Net::HTTPSuccess)
  rescue StandardError
    false
  end

  def json
    jsonArt = {}
    jsonArt['name'] = @name
    jsonArt['slug'] = @slug
    jsonArt['sku'] = @sku
    jsonArt['type'] = @type
    jsonArt['regular_price'] = @regular_price
    jsonArt['description'] = @description
    jsonArt['categories'] = @categories
    jsonArt['manage_stock'] = @manage_stock
    jsonArt['stock_quantity'] = @stock_quantity
    jsonArt['stock_status'] = @stock_status
    jsonArt['images'] = @images

    jsonArt
  end

  def get_image
    image = image_url(@ref)
    @images = [src: image] if image
  end

  def image_url(ccode)
    base_url = 'https://boutique.medicom.re/wp-content/uploads/images_medicom'
    name = ccode.gsub('/', '_')
    img_url = nil
    %w[jpg JPG png PNG jpeg JPEG].each do |ext|
      url = "#{base_url}/#{name}.#{ext}"
      if image_exists?(url)
        img_url = url
        break
      end
    end
    img_url
  end
end
