# require 'net/ftp'
require 'multi_xml'
require 'json'
require 'xmlsimple'
require 'without_accents'

def initFtp
  puts 'Récupération des fichiers Codial'
  Net::FTP.open(Config['ftp']['ip']) do |ftp|
    ftp.login Config['ftp']['user'], Config['ftp']['pass']
    ftp.chdir(Config['ftp']['folder'])
    files = ftp.list
    files_imports = []

    files.each do |file|
      if file.split(/\s+/)[0][0, 1] != 'd'
        ftp.getbinaryfile(file.split.last, Config['ftp']['folder_root'] + file.split.last)
        files_imports.push(Config['ftp']['folder_root'] + file.split.last)
      end
    end
  end
end

def import_CAT(xml_data)
  categories_ids = []

  # load json categories
  if File.exist?(Config['save']['folder'] + '/categories.json')
    categories_ids = JSON.parse(File.read(Config['save']['folder'] + '/categories.json'))
  else
    # create folder save
    Dir.mkdir(Config['save']['folder']) unless File.exist?(Config['save']['folder'])
  end

  xml_data['WINDEV_TABLE']['TableHierarchique1'].each do |cat|
    # CAT_Convert
    jsonCat = CAT_Convert(cat)
    # put
    response = go_woocommerce('post', 'products/categories', jsonCat)

    if response['code'] == 'term_exists'
      puts "Categorie: #{cat['categoryid']}\n"
      puts response['message'] + "\n"
    else
      infoId = {}
      infoId['id_codial'] = cat['categoryid'].to_i
      infoId['id_Woocommerce'] = response['id'].to_i
      categories_ids.push(infoId)
    end
  end

  File.write(Config['save']['folder'] + '/categories.json', JSON.pretty_generate(categories_ids))

  puts "CAT Finish\n"
end

def import_article(art)
  woo_product = find_product(art['ccode'].first)
  codial_product = CodialProduct.new(art)
  if !woo_product
    # Création du nouvel article
    codial_product.get_image
    response = go_woocommerce('post', 'products', codial_product.json)
    return 'create' if response

    puts "! #{i}/#{xml_count} Article: #{art['cname']} erreur lors de la création !"
    'error'

  else
    diff = products_differences(woo_product, codial_product)
    return 'same' if diff.empty?

    # Mise à jour de l'article
    response = go_woocommerce('put', "products/#{woo_product['id']}", diff)
    return 'update' if response

    'error'

    # Rien à faire

  end
end

def import_articles(xml_data)
  puts 'Importation des articles Codial'

  xml_count = xml_data['EXPORT'].count
  i = 1
  errors = 0
  updates = 0
  creates = 0
  sames = 0
  xml_data['EXPORT'].each do |art|
    res = import_article(art)
    case res
    when 'create'
      puts "+ #{i}/#{xml_count} Article: #{art['cname']} a été créé\n"
      creates += 1
    when 'update'
      puts "~ #{i}/#{xml_count} Article: #{art['cname']} mis à jour"
      updates += 1
    when 'same'
      puts "= #{i}/#{xml_count} Article: #{art['cname']} existe déjà, rien à faire"
      sames += 1
    else
      puts "! #{i}/#{xml_count} Article: #{art['cname']} erreur lors de la mise à jour !"
      errors += 1
    end
    i += 1
  end
  deletes = delete_old_articles(xml_data['EXPORT'])
  puts 'Importation terminée'
  puts "Création : #{creates}"
  puts "Mise à jour : #{updates}"
  puts "Erreurs : #{errors}"
  puts "Non touchés : #{sames}"
  puts "Suppressions : #{deletes}"
  puts "Total : #{creates + updates + errors + sames}"
end

def import_from_xml
  # file = File.join(Config['ftp']['folder_root'], 'MEDICOM_EXPORT_ART_1.xml')
  file = File.join('datas', 'MEDICOM_EXPORT_ART_1.xml')
  # xml_data = MultiXml.parse(File.read(file))
  # xml_data = Nokogiri::XML(File.read(file))
  xml_data = XmlSimple.xml_in(file)
  # p xml_data
  import_articles(xml_data)
end

def import_article_from_xml(ccode)
  # file = File.join(Config['ftp']['folder_root'], 'MEDICOM_EXPORT_ART_1.xml')
  file = File.join('datas', 'MEDICOM_EXPORT_ART_1.xml')
  xml_data = MultiXml.parse(File.read(file))
  xml_data['TABLE']['EXPORT'].each do |art|
    next unless art['ccode'] == ccode

    puts "Lancement de la récupération de l'article #{art['cname']}"
    res = import_article(art)
    case res
    when 'create'
      puts "+ 1/1 Article: #{art['cname']} a été créé"
    when 'update'
      puts "~ 1/1 Article: #{art['cname']} mis à jour"
    when 'same'
      puts "= 1/1 Article: #{art['cname']} déjà à jour, rien à faire"
    else
      puts "! 1/1 Article: #{art['cname']} erreur lors de la mise à jour !"
    end
  end
end
