require 'woocommerce_api'

Config = YAML.load_file('./config/config.yml')

Woocommerce = WooCommerce::API.new(
  Config['woocommerce']['url'],
  Config['woocommerce']['api_ck'],
  Config['woocommerce']['api_cs'],
  {
    wp_api: true,
    version: 'wc/v3'
  }
)

def go_woocommerce(method, endpoint, data = {})
  response = Woocommerce.get(endpoint, data) if method == 'get'
  response = Woocommerce.put(endpoint, data) if method == 'put'
  response = Woocommerce.post(endpoint, data) if method == 'post'
  response = Woocommerce.delete(endpoint, data) if method == 'delete'
  response = Woocommerce.options(endpoint) if method == 'options'

  # https://developer.mozilla.org/fr/docs/Web/HTTP/Status
  if response.code != 200 && response.code != 201
    begin
      puts "#{response.code} #{response.message}"
      puts response['message']
    rescue StandardError
      puts 'erreur inattendue'
    end
  end

  begin
    response.parsed_response
  rescue StandardError
    false
  end
end

def all_products(only_visible = true)
  per_page = 100
  nb_products = 0
  Woocommerce.get('reports/products/totals').parsed_response.each do |totals|
    nb_products = totals['total'].to_i if totals['slug'] == 'simple'
  end

  max_pages = (nb_products / per_page).to_i + (nb_products.modulo(per_page) > 0 ? 1 : 0)

  products = []
  options = { per_page: per_page }
  options.merge!({ catalog_visibility: 'visible' }) if only_visible
  (1..max_pages).each do |page|
    products += Woocommerce.get("products?page=#{page}&per_page=#{per_page}").parsed_response
  end

  products
end

puts "Récupération de la liste d'articles de Woocommerce"
Products = all_products(false)

def product_already_exist?(product_code)
  # product_name.gsub!('&', '&amp;')
  # product_name.gsub!('&amp;#8217;', '&#8217;')
  c = Products.select { |p| p['sku'] == product_code }.count
  c.positive?
end

def find_product(product_code)
  # product_code.gsub!('&', '&amp;')
  # product_code.gsub!('&amp;#8217;', '&#8217;')
  Products.select { |p| p['sku'] == product_code }.first
end

def products_differences(woo_product, codial_product)
  diff = {}
  jsonArt = codial_product.json
  woo_desc = woo_product['description'].gsub('</p>', '').gsub('\\n', '').gsub('&nbsp;', '').gsub('&gt;', '').gsub('&#339;', '').gsub('&#8230;', '').gsub(
    '&#8482;', ''
  )
  codial_desc = jsonArt['description'].to_s.gsub('</p>', '').gsub('\\n', '').gsub('&nbsp;', '').gsub('&gt;', '').gsub('&#339;', '').gsub('&#8230;', '').gsub('&#8482;', '').gsub('&', '&amp;').gsub(
    'text-decoration:underline \\', 'text-decoration:underline\\'
  )
  diff['description'] = jsonArt['description'].to_s unless woo_desc == codial_desc
  diff['name'] = jsonArt['name'] unless woo_product['name'] == jsonArt['name']
  diff['stock_quantity'] = jsonArt['stock_quantity'] unless woo_product['stock_quantity'] == jsonArt['stock_quantity']
  diff['regular_price'] = jsonArt['regular_price'] unless woo_product['regular_price'] == jsonArt['regular_price']

  if woo_product['images'].empty?
    codial_product.get_image
    jsonArt = codial_product.json
    diff['images'] = jsonArt['images'] unless jsonArt['images'].empty?
  end
  # if !diff.empty?
  #     puts "\nwoo_product"
  #     p woo_desc
  #     puts "\ncodial_product"
  #     p codial_desc
  # end
  diff
end

def doublons
  Products.each do |p|
    puts p['name'] if Products.count { |el| el['name'] == p['name'] } > 1
  end
  true
end

def delete_old_articles(xml)
  deletes = 0
  Products.each do |p|
    find = false
    xml.each do |art|
      find = true if art['ccode'].first == p['sku']
    end
    unless find
      if go_woocommerce('delete', "products/#{p['id']}")
        deletes += 1
        puts "- Suppression de l'article: #{p['name']}"
      else
        puts "! Article: #{p['name'].first} erreur lors de la suppression !"
      end
    end
  end
  deletes
end
