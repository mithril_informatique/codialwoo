require 'optparse'
require 'yaml'
require 'uri'
require './woocommerce'
require './export_csv'
require './import_xml'
require './codial_product'

$VERBOSE = nil # Supprime les warn de woocommerce_api

if __FILE__ == $0
  options = {}
  ARGV.push('-h') if ARGV.empty?
  OptionParser.new do |opts|
    opts.banner = 'Usage : ' + $PROGRAM_NAME + ' options'

    opts.on('-e', '--export-orders [path]',
            "Exporter vers un fichier CSV l'ensemble des commandes passées depuis le dernier export.") do |path|
      if path.nil?
        puts 'Il manque le chemin de destination !'
      else
        orders = goWoocommerce('get', 'orders', { status: 'processing' })
        date = Time.now.strftime('%Y%m%d_%H%M')
        name = path + '/export_cmd_codial_' + date + '.csv'
        export_orders_to_CSV(name, orders)
      end
    end

    opts.on('-f', '--ftp', 'Start Client Ftp') do
      # initFtp
      import_from_xml
    end

    opts.on('-pCCODE', '--product=CCODE', 'Import a specific product from Codial CCODE') do |p|
      initFtp
      import_article_from_xml(p)
    end
  end.parse!
end
